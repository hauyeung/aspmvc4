﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace csharp4_lesson13.Models
{
    public class Sale
    {
        public double amount { get; set; }
        public double percentage { get; set; }
    }
}
