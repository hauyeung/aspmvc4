﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using csharp4_lesson13.net.webservicex.www;
using System.Xml.Linq;
using System.Xml;

namespace csharp4_lesson13.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult soap()
        {
            StockQuote stock = new StockQuote();
            string msft = stock.GetQuote("msft");
            var ms = XElement.Parse(msft);
            XmlDocument msxml = new XmlDocument();
            msxml.LoadXml(ms.FirstNode.ToString());
            var sym =  msxml.GetElementsByTagName("Symbol");
            for (int i=0; i < sym.Count; i++)
            {
                ViewData["msname"] = sym[i].InnerText;
            }
            var last = msxml.GetElementsByTagName("Last");
            for (int i = 0; i < last.Count; i++)
            {
                ViewData["msprice"] = last[i].InnerText;
            }

            string znga = stock.GetQuote("znga");
            var zn = XElement.Parse(znga);
            XmlDocument znxml = new XmlDocument();
            msxml.LoadXml(zn.FirstNode.ToString());
            var znsym = msxml.GetElementsByTagName("Symbol");
            for (int i = 0; i < znsym.Count; i++)
            {
                ViewData["znname"] = znsym[i].InnerText;
            }
            var znlast = msxml.GetElementsByTagName("Last");
            for (int i = 0; i < znlast.Count; i++)
            {
                ViewData["znprice"] = znlast[i].InnerText;
            }
            
            return View();
        }
    }
}
