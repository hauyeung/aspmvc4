﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using csharp4_lesson13.Models;

namespace csharp4_lesson13.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values/5
        public HttpResponseMessage Get(double amount, double percentage)
        {
            Sale s = new Sale();
            s.amount = amount;
            s.percentage = percentage;
            return Request.CreateResponse(HttpStatusCode.OK,s.amount*(100-s.percentage)*0.01);
        }

        // POST api/values
        [HttpPost]
        public HttpResponseMessage Post(Sale s)
        {            
            var resp =  Request.CreateResponse(HttpStatusCode.OK, s.amount*(100-s.percentage)*0.01);
            return resp;
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}